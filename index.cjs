const items = require('./3-arrays-vitamins.cjs');
const getAvailableItems = require ('./problem1.cjs');
const getOnlyVitC = require ('./problem2.cjs');
const getVitA = require ('./problem3.cjs');
const getByVit = require('./problem4.cjs');
const sortByVit = require('./problem5.cjs');
//Problem1:
let availableList = getAvailableItems(items);
// console.log(availableList);

//Problem 2:
let vitC = getOnlyVitC(items);
// console.log(vitC);

//Problem 3:
let vitA = getVitA(items);
// console.log('Vitamin A: ',vitA);

//Problem 4:
let listByVit = getByVit(items);
// console.log(listByVit)

//Problem 5:
let vitSorted = sortByVit(items);
console.log(vitSorted);