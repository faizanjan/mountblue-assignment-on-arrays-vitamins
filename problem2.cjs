function getOnlyVitC(items){
    let result = items.filter(item=>{
        let contents = item.contains.split(',');
        return (contents.length===1 && contents.includes('Vitamin C'))
    })
    return result;
}

module.exports = getOnlyVitC;