function getByVit(items) {
    const result = items.reduce((acc, item) => {
        let contents = item.contains.split(',');
        for (let vit of contents) {
            vit = vit.trim();
            if (acc[vit]) acc[vit].push(item['name']);
            else acc[vit] = [item['name']];
        }
        return acc;
    }, {})
    return result;
}

module.exports = getByVit