
function getAvailableItems (items){
    let availableItems = items.filter(item=>item.available)
    return availableItems;
}

module.exports = getAvailableItems;